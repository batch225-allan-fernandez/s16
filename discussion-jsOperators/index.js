// Assignment Operators
		// Basic Assignment Operator (=)
        // The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
		let assignmentNumber = 8;
		console.log(assignmentNumber);

		// Addition Assignment Operator

		let totalNumber = assignmentNumber + 2;
		console.log("Result of addition assignment operator: " + totalNumber);

		// Arithmetic Operator (+, -, *, /, %)

		let x = 235;
		let y = 2358;

		let sum = x + y;
		console.log("Result of addition operator: " + sum);

		let difference = x - y;
		console.log("Result of substraction operator: " + difference);

		let product = x * y;
		console.log("Result of multiplication operator: " + product);

		let quotient = y / x;
		console.log("Result of quotient operator: " + quotient);

		let remainder = y % x;
		console.log("Result of modulo operator: " + remainder);


		// Multiple Operators and Parenthesis (MDAS and PEMDAS)
		/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/

		let mDas = 1 + 2 - 3 *4 / 5;
		console.log("Result of Mdas operation: " + mDas);

		let mDas2 =  6 / 2 * (1 + 2);
		console.log("Result of Mdas2 operation: " + mDas2);

		let pemDas = 1 + (2-3) * (4/5);
		console.log(pemDas)


		// Comparison Operator

		// Equality Operator (==)

		/*
			-checks whether the operands are equal or have the same content
			-Note: = is an assignment operator
			-Note: == is for equality operator
			-attempts to convert and cpmpare operands of different data types
			- True = 1
			- False = 0


			*/

		let juan = 'juan';

		console.log(1 == 1);
		console.log(1 == 2);
		console.log(1 == '1');
		console.log(0 == false);
		//Compares two strings that are the same 
		console.log('juan' == 'juacn');
		//Compares a string with the variable "juan" declared above 
		console.log('juan' == juan);

		// Inequality Operator (!=)
		/* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
        */

        console.log(1 != 1);
        console.log(1 != 2);
        console.log(1 != '1');
        console.log(0 != false);
        console.log('juan' != 'juan');
        console.log('juan' != juan);

        // Strict Equality Operator (===)

        /* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
        */

        console.log(1 === 1);
        console.log(1 === 2);
        console.log(1 === '1');
        console.log(0 === false);
        console.log('juan' === 'juan');
        console.log('juan' === juan);

        // Relational Operators

        //Some comparison operators check whether one value is greater or less than to the other value.

        let a = 50;
        let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65
        //LT or Less Than operator ( < )
        let isLessThan = a < b; // 50 < 65
        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual)


        // Logical Operators (&&, ||, !)

        	// Logical Operator (&& - AND Operator)
       		// Return true if all operands are true
        	// See "AND truth table" for reference

        	/*
        	1 && 1 = true
        	1 && 0 = false
        	0 && 1 = false
        	0 && 0 = false
        	*/

        let isLegalAge = true;
        let isRegistered  = false;

        

        let allRequirementsMet = isLegalAge && isRegistered;
        console.log("Result of Logical AND Operator: " + allRequirementsMet);

		// Logical Or Operator (|| - Double Pipe)
			// Returns true if one of the operands are true 

			// 1 || 1 = true
			// 1 || 0 = true
			// 0 || 1 = true
			// 0 || 0 = false

		let someRequirementsMet = isLegalAge || isRegistered; 
		console.log("Result of logical OR Operator: " +someRequirementsMet);

		// Logical Not Operator (! - Exclamation Point)
        	// Returns the opposite value 
        	
        let someRequirementsNotMet = !isRegistered;
        console.log("Result of logical NOT Operator: " + someRequirementsNotMet);